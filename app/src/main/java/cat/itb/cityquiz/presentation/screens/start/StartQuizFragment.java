package cat.itb.cityquiz.presentation.screens.start;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.question.CityQuizViewModel;

public class StartQuizFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);

        mViewModel.getGame().observe(this, this::onChanged);
    }

    private void onChanged(Game game) {
        if(game != null) navigateToQuiz();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.startBtn).setOnClickListener(this::startQuiz);
    }

    private void startQuiz(View view) {
        mViewModel.startQuiz();
    }

    public void navigateToQuiz(){
        Navigation.findNavController(getView()).navigate(R.id.start_to_question);
    }
}
