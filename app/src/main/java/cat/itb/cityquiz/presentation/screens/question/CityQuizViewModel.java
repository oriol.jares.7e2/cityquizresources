package cat.itb.cityquiz.presentation.screens.question;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {

    private GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    private int MAX_TIME = 5000;
    private AnswerCountDownTimer countdown = new AnswerCountDownTimer(MAX_TIME);

    private MutableLiveData<Game> gameLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> countdownLiveData = new MutableLiveData<>();

    public CityQuizViewModel() {
        countdown.setTimerChangedListener(this::onTimerChangedListener);
    }

    private void onTimerChangedListener(int i) {
        countdownLiveData.postValue(i);
    }

    public LiveData<Game> getGame() {
        return gameLiveData;
    }

    public MutableLiveData<Integer> getTimer() {
        return countdownLiveData;
    }

    public void startQuiz(){
        Game game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        countdown.start();
        gameLiveData.postValue(game);
    }

    public void answerQuestion(int response){
        Game game = gameLogic.answerQuestions(gameLiveData.getValue(), response);
        gameLiveData.postValue(game);
        countdown.restart();
    }

    public void skipQuestion(){
        Game game = gameLogic.skipQuestion(gameLiveData.getValue());
        gameLiveData.postValue(game);
        countdown.restart();
    }
}
