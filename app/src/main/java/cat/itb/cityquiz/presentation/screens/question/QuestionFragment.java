package cat.itb.cityquiz.presentation.screens.question;

import androidx.lifecycle.ViewModelProviders;

import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;

public class QuestionFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private ImageView img;
    private ProgressBar progressBar;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        Game game = mViewModel.getGame().getValue();
        display(game);

        mViewModel.getTimer().observe(this, this::onTimerChanged);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onTimerChanged(Integer integer) {
        progressBar.setProgress(integer);
        if (progressBar.getProgress() == 0){
            mViewModel.skipQuestion();
        }
    }


    private void onGameChanged(Game game) {
        if (game.isFinished()){
            navigateToGameResult();
        } else {
            display(game);
        }
    }

    private void display(Game game) {
        Question currentQuestion = game.getCurrentQuestion();
        List<City> cityList = currentQuestion.getPossibleCities();

        btn1.setText(cityList.get(0).getName());
        btn2.setText(cityList.get(1).getName());
        btn3.setText(cityList.get(2).getName());
        btn4.setText(cityList.get(3).getName());
        btn5.setText(cityList.get(4).getName());
        btn6.setText(cityList.get(5).getName());

        progressBar.setProgress(1000);

        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        img.setImageResource(resId);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = getView().findViewById(R.id.progressBar);

        img = getView().findViewById(R.id.questionImage);

        btn1 = getView().findViewById(R.id.button1);
        btn1.setTag(0);
        btn1.setOnClickListener(this::answerQuestion);

        btn2 = getView().findViewById(R.id.button2);
        btn2.setTag(1);
        btn2.setOnClickListener(this::answerQuestion);

        btn3 = getView().findViewById(R.id.button3);
        btn3.setTag(2);
        btn3.setOnClickListener(this::answerQuestion);

        btn4 = getView().findViewById(R.id.button4);
        btn4.setTag(3);
        btn4.setOnClickListener(this::answerQuestion);

        btn5 = getView().findViewById(R.id.button5);
        btn5.setTag(4);
        btn5.setOnClickListener(this::answerQuestion);

        btn6 = getView().findViewById(R.id.button6);
        btn6.setTag(5);
        btn6.setOnClickListener(this::answerQuestion);
    }

    private void navigateToGameResult(){
        Navigation.findNavController(getView()).navigate(R.id.question_to_score);
    }

    private void answerQuestion(View view) {
        int number = (Integer) view.getTag();
        mViewModel.answerQuestion(number);
    }



}
