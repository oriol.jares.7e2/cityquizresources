package cat.itb.cityquiz.presentation.screens.score;

import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.question.CityQuizViewModel;

public class ScoreFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    private TextView score;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);

        mViewModel.getGame().observe(this, this::onChanged);
    }

    private void onChanged(Game game) {
        if(game.isFinished()){
            display(game);
        } else {
            navigateToQuiz();
        }
    }

    public void display(Game game){
        score.setText(game.getNumCorrectAnswers() + "");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        score = getView().findViewById(R.id.scoreResult);

        view.findViewById(R.id.playAgainBtn).setOnClickListener(this::playAgain);
    }

    private void playAgain(View view) {
        mViewModel.startQuiz();
    }

    public void navigateToQuiz(){
        Navigation.findNavController(getView()).navigate(R.id.score_to_question);
    }

}
